﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
namespace Spaceship
{
    public class Player : MonoBehaviour
    {
        
        [SerializeField] GameObject player;
        [SerializeField] int Hp;
        
        

        private void Start()
        {
            
        }
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.CompareTag("Enemiesbullet"))
            {
               Hp = Hp - 20;
                if (Hp <= 0)
                {
                    Destroy(player);
                    SceneManager.LoadScene("Mainmenu");
                }
            }
            
        }
    }
}
 
    

