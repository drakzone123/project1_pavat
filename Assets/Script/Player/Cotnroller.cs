﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cotnroller : MonoBehaviour
{
    [SerializeField] float speedPlayer;
    [SerializeField] GameObject playerBullet;
    [SerializeField] Transform spownBullet;
    private Rigidbody2D rb2D;
    private Vector2 moveVelocity;
    
    void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
    }

    
    void Update()
    {
        Controller_player();
        Shoot();
    }
    private void FixedUpdate()
    {
        Calcurate_position();
    }
    private void Controller_player()
    {
        Vector2 moveInput = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        moveVelocity = moveInput.normalized * speedPlayer;
    }
    private void Calcurate_position()
    {
        rb2D.MovePosition(rb2D.position + moveVelocity * Time.fixedDeltaTime);
    }
    private void Shoot()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Instantiate(playerBullet, spownBullet.position, spownBullet.rotation);
        }
    }
}
