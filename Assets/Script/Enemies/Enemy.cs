﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Enemy : MonoBehaviour
{
    [SerializeField] GameObject enemyship;
    [SerializeField] int Hp_enemy = 100;
    private int score;
    
    // Update is called once per frame
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Playerbullet"))
        {
            Hp_enemy = Hp_enemy - 20;
            if (Hp_enemy <= 0)
            {
                Destroy(enemyship);
                SceneManager.LoadScene("Game_level2");
                
            }
        }

    }
}
