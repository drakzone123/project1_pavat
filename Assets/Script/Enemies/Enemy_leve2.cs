﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Enemy_leve2 : MonoBehaviour
{
    [SerializeField] GameObject enemyship_level2;
    [SerializeField] int Hp_enemy = 100;


    // Update is called once per frame

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Playerbullet"))
        {
            Hp_enemy = Hp_enemy - 20;
            if (Hp_enemy <= 0)
            {
                Destroy(enemyship_level2);


            }
        }

    }
}