﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bulleplayer : MonoBehaviour
{
    [SerializeField] float speedBullet;
    private Rigidbody2D bullet;
    private void Start()
    {
        bullet = GetComponent<Rigidbody2D>();
        bullet.velocity = new Vector2(0, speedBullet);
    }

    
    
}
